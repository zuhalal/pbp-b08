from django.contrib import admin

# Register your models here.
from .models import Benefit

admin.site.register(Benefit)