from django.urls import path
from .views import index, loginpage, register, logoutPage,afterLogin, test, login_with_flutter, register_with_flutter, logout_wiht_flutter

urlpatterns = [
    path('', index, name='index'),
    path('login', loginpage, name="login"),
    path('logout', logoutPage, name="logout"),
    path('register', register, name="register"),
    path('home', afterLogin, name='afterLogin'),
    path('login2', login_with_flutter, name="login_flutter"),
    path('register2', register_with_flutter, name="register_flutter"),
    path('logout2', logout_wiht_flutter, name="logout_flutter")
]