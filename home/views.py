from django.http.response import HttpResponseRedirect, JsonResponse
import requests 
import json
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, UserModel
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.backends import UserModel
# Create your views here.
from .forms import CreateUserForm
from .models import Benefit
from django.http import JsonResponse
import json


def index(request):
    if request.user.is_authenticated:
        return redirect('afterLogin')
    else:
        benefit = Benefit.objects.all().values()
        response = {'benefits' : benefit}
        return render(request, 'home.html', response)

@login_required(login_url='login')
def afterLogin(request):
    response = {}
    return render(request, 'homeAfterLogin.html', response)

def register(request):
    if request.user.is_authenticated:
        return redirect('afterLogin')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                return redirect('login')
        context = {
            'form' : form,
        }
        return render(request, 'register.html', context)

def loginpage(request):
    if request.user.is_authenticated:
        return redirect('afterLogin')
    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username = username, password = password)

            if user is not None:
                login(request, user)
                return redirect('afterLogin')
            else:
                messages.error(request, 'Username or Password is wrong')
                
        context = {}
        return render(request, 'login.html', context)

def logoutPage(request):
    logout(request)
    return redirect('login')

def test(request):
    return render(request, "tes.html")

@csrf_exempt
def login_with_flutter(request) :
    username = request.POST.get('username')
    password = request.POST.get('password')

    user = authenticate(username = username, password = password)
    print(user)

    if user is not None:
        if user.is_active:
            auth_login(request,user)
            return JsonResponse({"status": True, "username": request.user.username, "message": "Login Berhasil!"}, status=200)
        else:
            return JsonResponse({"status": False, "message": "Gagal login, Akun tidak bisa diakses"}, status=401)
    else :
        return JsonResponse({"status": False, "message": "Gagal login, Username/Password tidak sesuai"}, status=401)

@csrf_exempt
def register_with_flutter(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        print(data)

        username = data["username"]
        password = data["password1"]

        print("username " + username)
        print(password)

        newUser = UserModel.objects.create_user(
        username = username, 
        password = password,
        )

        newUser.save()
        return JsonResponse({"status": True, "message": "Berhasil Mendaftar Akun"}, status=200)
    else:
        return JsonResponse({"status": False, "message": "Gagal Mendaftar Akun"}, status=401)

@csrf_exempt
def logout_wiht_flutter(request):
    try:
        logout(request)
        return JsonResponse({
                "status": True,
                "message": "Successfully Logged out!"
            }, status=200)
    except:
        return JsonResponse({
          "status": False,
          "message": "Failed to Logout"
        }, status=401)