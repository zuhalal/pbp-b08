from django.db import models

# Create your models here.
from cloudinary.models import CloudinaryField

class Benefit(models.Model):
    image = CloudinaryField('image')
    caption = models.TextField()

