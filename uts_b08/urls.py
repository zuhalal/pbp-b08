"""uts_b08 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import include, path
from django.conf import settings
from django.views.static import serve
import home.urls as home
import kontak.urls as kontak
import donasi.urls as donasi
import kritik_saran.urls as kritikSaran 
import updateCovid.urls as updateCovid
import artikel.urls as artikel
import vaksin.urls as vaksin

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(home)),
    path('kritikSaran/', include(kritikSaran)),
    path('donasi/', include(donasi)),
    path('updateCovid/', include(updateCovid)),
    path('artikel/', include((artikel))),
    path('kontak/', include(kontak)),
    path('vaksin/', include(vaksin))
]

if settings.DEBUG:
    urlpatterns += [
        url(r'media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT
        })
    ]
