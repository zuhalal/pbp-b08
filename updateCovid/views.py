from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from .models import Note, Like, DataProvinsi
from .form import NoteForm
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json
from django.contrib.auth import get_user_model

def userJson(request):
    User = get_user_model()
    data = serializers.serialize('json', User.objects.all())
    return HttpResponse(data, content_type="application/json")


def updateCovid(request):
    notes = Note.objects.all().order_by('-published_date')
    prov = DataProvinsi.objects.all().order_by('urutan')
    form  = NoteForm
    response = {'notes': notes, 'form' : form, 'prov' : prov}
    if request.method == "POST":
        form  = NoteForm(request.POST)
        if form.is_valid():
            note = form.save(commit=False)
            note.Author = request.user
            note.save()
            return HttpResponseRedirect('/updateCovid#msg')
    
    return render(request, 'indexUC.html', response)

def like_note(request):
    user = request.user
    if request.method == "POST":
        post_id = request.POST.get('post_id')
        post_obj = Note.objects.all().get(id=post_id)

        if user in post_obj.like.all():
            post_obj.like.remove(user)
        else:
            post_obj.like.add(user)
    
        like, created = Like.objects.get_or_create(user=user, note_id=post_id)

        if not created:
            if like.value == 'Like':
                like.value = 'Unlike'
            else:
                like.value = 'Like'
        
        post_obj.save()
        like.save()
        
    return redirect('indexHarian')

def jsonHarapan(request):
    data = serializers.serialize('json', Note.objects.all())
    # extra = json.dumps({'username' : "aku"})
    return HttpResponse(data, content_type="application/json")

def jsonProv(request):
    data = serializers.serialize('json', DataProvinsi.objects.all())
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def add_mobile(request):
    if not request.user.is_authenticated:
        return JsonResponse({"error": "Add Failed"}, status=400)

    if request.method == 'POST':
        data = json.loads(request.body)
        pesan = data["pesan"]
        harapan = Note(Author=request.user, Message= pesan)
        harapan.save()

        return JsonResponse({"status": "success"}, status=200)
    
    else:
        return JsonResponse({"status": "error"}, status=401)

@csrf_exempt
def like_mobile(request):
    data = json.loads(request.body)
    post_obj = get_object_or_404(Note, id = data['id'])
    user = request.user

    if request.method == "POST":
        if user in post_obj.like.all():
            post_obj.like.remove(user)
        else:
            post_obj.like.add(user)
        
        like, created = Like.objects.get_or_create(user=user, note_id = data['id'])

        if not created:
            if like.value == 'Like':
                like.value = 'Unlike'
            else:
                like.value = 'Like'
            
        post_obj.save()
        like.save()
        return JsonResponse({"status": "success"}, status=200)

    return JsonResponse({"error": "Internal Server Error"}, status=500)
