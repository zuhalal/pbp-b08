from django.contrib import admin
from .models import Note,Like,DataProvinsi

# Register your models here.
admin.site.register(Note)
admin.site.register(Like)
admin.site.register(DataProvinsi)
