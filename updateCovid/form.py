from django import forms
from .models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ('Message',)    
        widgets = {
            'Message': forms.Textarea(attrs= {'rows':1, 'id' : 'form-msg','class': 'form-control text-primary-rh','placeholder' : 'Silahkan tuliskan harapan kalian untuk Indonesia di sini...'}),
        }


	


