from django.urls import path
from .views import updateCovid, jsonHarapan, like_note, jsonProv, add_mobile, like_mobile, userJson

urlpatterns = [
    path('', updateCovid, name='indexHarian'),
    path('jsonHarapan', jsonHarapan, name='json'),
    path('jsonProv', jsonProv, name='jsonProv'),
    path('jsonUser', userJson, name='jsonUser'),
    path('like', like_note, name='like-note'),
    path('addMobile', add_mobile, name='add-mobile'),
    path('likeMobile', like_mobile, name='like-note'),
]