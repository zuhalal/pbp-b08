from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Note(models.Model):
    Author = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True)
    Message = models.TextField() 
    published_date = models.DateTimeField(default=timezone.now)
    like = models.ManyToManyField(User, default=None, blank=True, related_name="like")

    def __str__(self):
        x = self.Message.split()
        y = x[0]
        return y
                
    @property
    def num_likes(self):
        likes = self.like.all().count()
        return likes

LIKE_CHOICES = (
    ('Like', 'Like'),
    ('Unlike', 'Unlike'),
)

class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name='like_user', default='')
    note = models.ForeignKey(Note, on_delete=models.CASCADE, null=True, blank=True, related_name='like_note', default='')
    value = models.CharField(choices=LIKE_CHOICES, default="Like", max_length=10)

    def __str__(self):
        return str(self.note)

class DataProvinsi(models.Model):
    kota = models.CharField(max_length=50)
    positif = models.CharField(max_length=50, default=0)
    sembuh = models.CharField(max_length=50, default=0)
    kematian = models.CharField(max_length=50, default=0)
    urutan = models.CharField(max_length=10)