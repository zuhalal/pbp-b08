from django import forms
from vaksin.models import Vaksin

class AddForum(forms.ModelForm):
    class Meta:
        model = Vaksin
        fields = ('judul', 'konten')
        labels = {
            'judul' : 'Judul :',
            'konten' : ''
        }
        widgets = {
            'judul' : forms.TextInput(
                attrs={
                    'class':'form-control border-primary-rh text-center',
                    'id':'judul-form',
                    'placeholder':'Masukkan Judul'
                })
        }
