from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

class Vaksin(models.Model):
    judul = models.CharField(max_length=100, default='')
    penulis = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, blank=True, default='')
    tanggal_publikasi = models.DateTimeField(default=timezone.now())
    konten =  RichTextField()
