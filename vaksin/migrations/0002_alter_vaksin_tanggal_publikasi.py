# Generated by Django 3.2.8 on 2021-12-31 00:50

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('vaksin', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vaksin',
            name='tanggal_publikasi',
            field=models.DateTimeField(default=datetime.datetime(2021, 12, 31, 0, 50, 34, 241126, tzinfo=utc)),
        ),
    ]
