from django.core import serializers
from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from vaksin.models import Vaksin
from vaksin.forms import AddForum
import json

def index_vaksin(request):
    kontens = Vaksin.objects.all().order_by('-tanggal_publikasi')
    response = {'konten': kontens}
    return render(request, 'index_vaksin.html', response)

@csrf_exempt
def get_all_forum(request):
    kontens = Vaksin.objects.all().order_by('-tanggal_publikasi')
    data = serializers.serialize('json', kontens)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def get_user_konten(request):
    kontens = Vaksin.objects.all().order_by('-tanggal_publikasi')
    konten_user = []

    for konten in kontens:
        if (konten.penulis == request.user):
            konten_user.append(konten)

    data = serializers.serialize('json', konten_user)
    return HttpResponse(data, content_type="application/json")

def detail_forum(request, konten_id, *args, **kwargs):
    kontens = get_object_or_404(Vaksin, id=konten_id)
    response = {'konten' : kontens}
    return render(request, 'detail_forum.html', response)

def get_absolute_url(konten):
    return '/vaksin/detail-forum/%i' % konten.id

@csrf_exempt
def add_forum(request):
    if request.user.is_authenticated:
        context = {}
        form = AddForum(request.POST or None, request.FILES or None)
        
        if form.is_valid() and request.method == 'POST':
            forms = form.save(commit=False)
            forms.penulis = request.user
            forms.save()
            return HttpResponseRedirect('/vaksin/#foruminformasi')

        context['form'] = form
        return render(request, 'add_forum.html', context)
    else:
        return HttpResponseRedirect('/login')  

@csrf_exempt
def add_forum_api(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            data = json.loads(request.body)
            judul = data['judul']
            konten = data['konten']
            kontens = Vaksin(judul=judul, penulis=request.user, konten=konten)
            kontens.save()
            return JsonResponse({'status': 'success'}, status=200)
        
        else:
            return JsonResponse({'status': 'error'}, status=400)
    else:
        return JsonResponse({'error': 'Unauthorized'}, status=401)

@csrf_exempt
def edit_forum(request, konten_id):
    kontens = get_object_or_404(Vaksin, id=konten_id)
   
    if request.user != kontens.penulis:
        return HttpResponseRedirect(get_absolute_url(kontens))
    
    elif request.user.is_authenticated and request.user == kontens.penulis:
        context = {}
        form = AddForum(instance = kontens)
        
        if request.method == 'POST':
            form = AddForum(request.POST, request.FILES, instance=kontens)
            if form.is_valid() and request.method == 'POST':
                form.save()
                return HttpResponseRedirect(get_absolute_url(kontens))
            
        context['form'] = form
        return render(request, 'edit_forum.html', context)
    
    else:
        return HttpResponseRedirect('/login')

@csrf_exempt
def edit_forum_api(request, konten_id):
    kontens = get_object_or_404(Vaksin, id = konten_id)

    if request.user != kontens.penulis:
        return JsonResponse({'error': 'Unauthorized'}, status=401)

    else:
        if request.method == 'POST':
            data = json.loads(request.body)
            new_judul = data['judul']
            new_konten = data['konten']

            kontens.penulis = request.user
            kontens.judul = new_judul
            kontens.konten = new_konten
            kontens.save()
            return JsonResponse({'status': 'success'}, status=200)
        
        return JsonResponse({'error': 'failed'}, status=400)

@csrf_exempt
def delete_forum(request, konten_id):
    kontens = get_object_or_404(Vaksin, id=konten_id)

    if request.user != kontens.penulis:
        return HttpResponseRedirect(get_absolute_url(kontens))
    
    elif request.user.is_authenticated and request.user == kontens.penulis:
        kontens.delete()
        return HttpResponseRedirect('/vaksin/#foruminformasi')
         
    else:
        return HttpResponseRedirect('/login')  
