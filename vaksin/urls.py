from django.urls import path
from vaksin.views import index_vaksin, get_all_forum, get_user_konten, detail_forum, add_forum, add_forum_api, edit_forum, edit_forum_api, delete_forum

urlpatterns = [
    path('', index_vaksin, name='index_vaksin'),
    path('all-forum', get_all_forum, name='all_forum'),
    path('user-konten', get_user_konten, name='user_konten'),
    path('detail-forum/<int:konten_id>', detail_forum, name='detail_forum'),
    path('add-forum', add_forum, name='add_forum'),
    path('add-forum-API', add_forum_api, name='add_forum_API'),
    path('edit-forum/<int:konten_id>', edit_forum, name='edit_forum'),
    path('edit-forum-API/<int:konten_id>', edit_forum_api, name='edit_forum_API'),
    path('delete-forum/<int:konten_id>', delete_forum, name='delete_forum')
]