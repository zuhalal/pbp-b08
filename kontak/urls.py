from django.urls import path
from .views import get_kontak, index, add_contact, add_contact_api

urlpatterns = [
    path('', index, name='index'),
    path('add/', add_contact, name='add_contact'),
    path('list_kontak', get_kontak, name='get_kontak'),
    path('addAPI', add_contact_api, name='add_contactAPI'),
    #url(r'^add/$', add_contact, name='add_contact'),
]
