from django import forms
from .models import Kontak

class ContactForm(forms.ModelForm):
    class Meta:
        model = Kontak
        fields ='__all__'
        #fields = '__all__'
        exclude = ('auth',) 