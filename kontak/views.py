import json
from django.shortcuts import render
from .models import Kontak 
from .forms import ContactForm
from .filters import ContactFilter
from django.core import serializers
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, HttpResponsePermanentRedirect
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt

def index(request):
    contact_list = Kontak.objects.all()
    myFilter = ContactFilter(request.GET, queryset=contact_list)
    contact_list = myFilter.qs
    paginator = Paginator(contact_list, 10)
    page_num = request.GET.get('page',1)
    try:
        page = paginator.page(page_num)
    except PageNotAnInteger:
        page = paginator.page(1)
    except EmptyPage:
        page = paginator.page(paginator.num_pages)
    if myFilter == True:
        return(HttpResponseRedirect('/kontak#TabelKontakPenting'))
    else:
        response = {'contact_list': page, 'myFilter' : myFilter}
        return render(request, 'kontak_index.html', response)

@login_required(login_url='login')
def add_contact(request):
    data = dict()

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            data['form_is_valid'] = True
            contact_list = Kontak.objects.all()
            data['html_contact_list'] = render_to_string('kontak_list.html', {
                'contact_list': contact_list
            })
        else:
            data['form_is_valid'] = False
    
    else:
        form = ContactForm()
    context = {'form': form}
    data['html_form'] = render_to_string('kontak_create.html',
        context,
        request=request,
    )
    return JsonResponse(data)

@csrf_exempt
def get_kontak(request):
    kontak = Kontak.objects.all()
    data = serializers.serialize('json', kontak)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def add_contact_api(request):
    if not request.user.is_authenticated:
        print("here")
        return JsonResponse({"error": "Unauthorized"}, status=401)

    if request.method == 'POST':

        data = json.loads(request.body)

        region = data["region"]
        provinsi = data["provinsi"]
        kategori = data['kategori']
        kota = data['kota']
        namakontak = data['namakontak']
        nomorkontak = data['nomorkontak']
        alamatkontak = data['alamatkontak']
        keterangan = data['keterangan']

        kontak = Kontak(region=region, provinsi = provinsi, kategori=kategori, kota=kota, namakontak=namakontak, nomorkontak=nomorkontak, alamatkontak=alamatkontak, keterangan=keterangan)
        kontak.save()

        return JsonResponse({"status": "success"}, status=200)

    else:
        return JsonResponse({"status": "error"}, status=400)
