from django.db import models
from django.contrib.auth.models import User
# Create your models here.

#class Kategori(models.Model):
    #name = models.CharField('Kategori', max_length=200, help_text='Isi kategori kontak (contoh: Rumah Sakit)')
    #def __str__(self):
        #"""String for representing the Model object (in Admin site etc.)"""
        #return self.name

#class Provinsi(models.Model):
    #name = models.CharField('Provinsi', max_length=30, help_text='30 karakter <a href="https://id.wikipedia.org/wiki/Provinsi_di_Indonesia">Nama Provinsi</a>')
    #def __str__(self):
        #"""String for representing the Model object (in Admin site etc.)"""
        #return self.name

class Kontak(models.Model):
    REGION = [
                ('Nasional', 'Nasional'),
                ('Jawa', 'Jawa'),
                ('Sumatra', 'Sumatra'),
                ('Kalimantan', 'Kalimantan'),
                ('Sulawesi', 'Sulawesi'),
                ('Bali', 'Bali'),
                ('Papua', 'Papua')
    ]

    KATEGORI = [
                ('Hotline COVID-19', 'Hotline COVID-19'),
                ('Rumah Sakit', 'Rumah Sakit'),
                ('Ambulans', 'Ambulans'),
                ('Bank Darah', 'Bank Darah'),
                ('Suplier Alat Kesehatan', 'Suplier Alat Kesehatan'),
                ('Lainnya', 'Lainnya')]
    
    PROVINSI = [('Aceh', 'Aceh'), ('Sumatera Utara', 'Sumatera Utara'), ('Sumatera Barat','Sumatera Barat'),
                ('Riau', 'Riau'), ('Jambi', 'Jambi'), ('Sumatera Selatan', 'Sumatera Selatan'), ('Bengkulu', 'Bengkulu'),
                ('Lampung', 'Lampung'), ('Kep. Bangka Belitung', 'Kep. Bangka Belitung'), ('Kep. Riau', 'Kep. Riau'),
                ('DKI Jakarta', 'DKI Jakarta'), ('Jawa Barat', 'Jawa Barat'), ('Jawa Tengah', 'Jawa Tengah'),
                ('DI. Yogyakarta', 'DI. Yogyakarta'), ('Jawa Timur', 'Jawa Timur'), ('Banten', 'Banten'), ('Bali', 'Bali'),
                ('NTB', 'NTB'), ('NTT', 'NTT'), ('Kalimantan Barat', 'Kalimantan Barat'), ('Kalimantan Selatan', 'Kalimantan Selatan'),
                ('Kalimantan Tengah', 'Kalimantan Tengah'), ('Kalimantan Timur', 'Kalimantan Timur'), ('Kalimantan Utara', 'Kalimantan Utara'),
                ('Sulawesi Barat', 'Sulawesi Barat'), ('Sulawesi Selatan', 'Sulawesi Selatan'), ('Sulawesi Tengah', 'Sulawesi Tengah'),
                ('Sulawesi Tenggara', 'Sulawesi Tenggara'), ('Sulawesi Utara', 'Sulawesi Utara'), ('Gorontalo', 'Gorontalo'), ('Maluku', 'Maluku'),
                ('Maluku Utara', 'Maluku Utara'), ('Papua', 'Papua'), ('Papua Barat', 'Papua Barat')]

    auth = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    region = models.CharField(max_length=50, choices=REGION)
    provinsi = models.CharField(max_length=100, choices=PROVINSI,)
    kategori = models.CharField(max_length=100, choices=KATEGORI)
    kota = models.CharField(max_length=100)
    namakontak = models.CharField(verbose_name="Nama Kontak", max_length=100)
    nomorkontak = models.CharField(verbose_name= "Nomor Kontak", max_length=20)
    alamatkontak = models.CharField(verbose_name="Alamat Kontak", max_length=500)
    keterangan = models.TextField()

    class Meta:
        ordering=['-pk', 'region', 'provinsi']

