$(document).ready(function(){
    $.ajax({
        type: "GET",
        url: "https://covid19.mathdro.id/api/countries/IDN/",
        data: {},
        success: function(result) {
            // console.log(result.lastUpdate.toString());
            var date = new Date(result.lastUpdate);
            // console.log(date);
            document.getElementById('confirmed').textContent = result['confirmed']['value'].toLocaleString('id');
            document.getElementById('recovered').textContent = result['recovered']['value'].toLocaleString('id');
            document.getElementById('deaths').textContent = result['deaths']['value'].toLocaleString('id');
            document.getElementById('lastUp').textContent = date.toLocaleString('id');
            }
        }
    );
})

$(document).ready(function(){
    var map = []
    $.ajax({
        type: "GET",
        url: "/updateCovid/jsonProv",
        data: {},
        success: function(result) {
            for (var i = 0; i < result.length; i++) {
                console.log(result[i]);
                insertTableData(result[i],i);
            }
        }
    })
})

const insertTableData = (map, x) => {
    return $(".tblprov").append(`
        <tr>
            <td id='no'>${x+1}</td>
            <td id='namaProv'>${map.fields.kota}</td>
            <td id='posProv'>${map.fields.positif}</td>
            <td id='sembProv'>${map.fields.sembuh}</td>
            <td id='meniProv'>${map.fields.kematian}</td>
        </tr>`)
}


$(document).ready(function(){
    $.ajax({
        type: "GET",
        url: "/updateCovid/json",
        success: function(result) {
            console.log(result);
            console.log(result.length);
            document.getElementById('jmlHar').textContent = result.length;
            }
        }
    );
})


// $(document).ready(function(){
//     $(".reply-popup").click(function(){
//       $(".reply-box").toggle();
//     });
//   });