$(function () {

  $(".js-create-contact").click(function () {
    $.ajax({
      url: '/kontak/add',
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-contact").modal("show");
      },
      success: function (data) {
        $("#modal-contact .modal-content").html(data.html_form);
      }
    });
  });

  $("#modal-contact").on("submit", ".js-contact-create-form", function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#contact-table tbody").html(data.html_contact_list);  
          $("#modal-contact").modal("hide");  
        }
        else {
          $("#modal-contact .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  });

});



