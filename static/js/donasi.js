const insertAllCarouselData = (result) => {
  function getCompressedText(text) {
    if (text.length > 35) {
      return text.slice(0, 35) + "..";
    }
    else return text
  }
  return $("#semua-carousel").append(`
    <a class="swiper-slide card-donasi word-break" href="/donasi/detail/${result.pk}">
    <div style="height: 50%; max-height: 50%">
      <img src="${result.fields.link_gambar}" alt="Rumah Harapan🏠" />
    </div>
    <div style="height: 50%" class="card-carousel-content">
      <div style="text-align: justify; color: #59a5db">
        <p class="word-break">Penggalang: <span>${result.fields.penggalang}</span></p>
      </div>
      <div style="text-align: justify;" class="deskripsi-donation">
          <p id="textDeskripsi" class="word-break" style="color:black;">${getCompressedText(result.fields.title)}</p>
      </div>
      <div style="text-align: justify" class="status-donasi">
        <div style="color: #27ae60" class="bottom-card-left">
          <p>Target:</p>
          <p class="word-break">Rp${result.fields.target}</p>
        </div>
        <div class="bottom-card-right">
          <div>
            <p style="color:black;">Tenggat Waktu:</p>
            <p class="word-break" style="color:black;">${result.fields.due_date}</p>
          </div>
        </div>
      </div>
    </div>
  </a>`);
};

const insertMyCarouselData = (result) => {
  function getCompressedText(text) {
    if (text.length > 35)
      return text.slice(0, 35) + "..";
    else return text
  }
  return $("#carousel-saya").append(`
  <div class="swiper-slide card-donasi">
  <div style="height: 50%; max-height: 50%">
    <img src="${result.fields.link_gambar}" alt="Rumah Harapan🏠" />
  </div>
  <div style="height: 50%" class="card-carousel-content">
    <div style="text-align: justify">
      <p class="word-break">
        ${getCompressedText(result.fields.title)}
      </p>
    </div>
    <div class="btn-myDonasi">
      <div class="btn-lihat-myDonasi">
        <a href="/donasi/detail/${result.pk}" class="aBtn" >
          <button class="bg-primary-rh border-primary-rh text-white btn-rh btn-lihat btn-primary-rh">
            <p class="p20px">Lihat</p>
          </button>
        </a>
      </div>
      <div class="btn-edit-myDonasi">
        <a href="/donasi/edit/${result.pk}" class="aBtn">
          <button class="border-secondary-rh btn-rh btn-scnd-rh"
          id="openEdit"
          value=${result.pk}
          >
            <p class="p20px">Edit</p>
          </button>
        </a>
      </div>
      <div class="btn-hapus-myDonasi">
        <button
          class="bg-error-rh border-error-rh text-white btn-rh btn-danger-rh"
          id="openDelete"
          data-bs-toggle="modal"
          data-bs-target="#modalDelete"
          value=${result.pk}
        >
          <p class="p20px">Delete</p>
        </button>
      </div>
    </div>
  </div>
</div>`);
};

const getAllCarouselData = () => {
  return $(document).ready(function () {
    $.ajax({
      url: "/donasi/all_donasi",
      success: function (results) {
        results.map((result) => {
          insertAllCarouselData(result);
        });
      },
    });
  });
};

const getMyCarouselData = () => {
  return $(document).ready(function () {
    $.ajax({
      url: "/donasi/my_donasi",
      success: function (results) {
        results.map((result) => {
          insertMyCarouselData(result);
        });
      },
    });
  });
};

// pk digunakan buat identifier unik antara satu message dengan yang lain buat yang mau kita delete
let pk = "";
$(document).ready(function () {
  $("#carousel-saya").on('click', '#openDelete', function() {
      // kita dapetin pk dengan cara mengambil value dari masing-masing tabel
      pk = $(this).attr("value");
    });
});
$(document).ready(function () {
  $("#delete").click(function () {
    $.ajax({
      type: 'POST',
      url: `/donasi/delete/${pk}`,
      data: {
            // csrf token for security
            'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
      },
      success: function (result) {
        // sebelum kita refresh tabel, kita kosongin dulu tablenya
        $("#carousel-saya").empty();
        $("#semua-carousel").empty();

        // saat sudah bersih, kita dapetin ulang datanya
        getAllCarouselData();
        getMyCarouselData();
      },
    });
  });
});

getAllCarouselData();
getMyCarouselData();