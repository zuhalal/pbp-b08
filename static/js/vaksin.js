$(document).ready(function(){
    $.ajax({
        type: "GET",
        url: "https://vaksincovid19-api.vercel.app/api/vaksin",
        data: {},
        success: function(result) {   
            console.log(result);
            document.getElementById('totalsasaran').textContent = result['totalsasaran'].toLocaleString('id');
            document.getElementById('sasaransdmk').textContent = result['sasaranvaksinsdmk'].toLocaleString('id');
            document.getElementById('sasaranlansia').textContent = result['sasaranvaksinlansia'].toLocaleString('id');
            document.getElementById('sasaranpetugas').textContent = result['sasaranvaksinpetugaspublik'].toLocaleString('id');
            document.getElementById('dosis1').textContent = result['vaksinasi1'].toLocaleString('id');
            document.getElementById('dosis2').textContent = result['vaksinasi2'].toLocaleString('id');
            var tanggal = new Date(result.lastUpdate);
            let formattedDate = new Intl.DateTimeFormat("id", {
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: '2-digit'
                }).format(tanggal);
            let formattedTime = new Intl.DateTimeFormat("id", {
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric',
                    timeZoneName: 'short'
                }).format(tanggal);
            document.getElementById('lastUp').textContent = formattedDate + ' pukul ' + formattedTime;
            }
        }
    );
})