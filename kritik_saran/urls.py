from django.urls import path
from .views import add_kritik, add_kritik_saran, get_kritik, index, load_more

urlpatterns = [
    path('', index, name='index'),
    path('form', add_kritik, name='form'),
    path('load', load_more, name='load'),
    path('json', get_kritik, name='json'),
    path('addKritik', add_kritik_saran, name='add-kritik'),
]
