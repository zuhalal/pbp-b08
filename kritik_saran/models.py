from django.db import models

# Create your models here.
class KritikSaran(models.Model):
    nama = models.CharField(max_length=30)
    kritik_saran = models.TextField(max_length=600)
    waktu_post = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-waktu_post']
