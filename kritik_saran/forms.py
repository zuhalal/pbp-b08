from django import forms
from .models import KritikSaran

class KritikSaranForm(forms.ModelForm):
    class Meta:
        model = KritikSaran
        fields = "__all__"

    nama = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'nama (boleh nama samaran)'})
    )

    kritik_saran = forms.CharField(widget=forms.Textarea(
        attrs={'placeholder': 'kritik dan saran...'})
    )
     