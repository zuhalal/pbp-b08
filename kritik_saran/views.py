from django.shortcuts import render
from .models import KritikSaran
from django.http.response import HttpResponse, HttpResponseRedirect
from .forms import KritikSaranForm
from django.http import JsonResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.
def index(request):
    kritik_saran = KritikSaran.objects.all()[0:3]
    total_obj = KritikSaran.objects.count()
    return render(request, 'index.html', context={'KritikSaran': kritik_saran, 'total_obj': total_obj})

def add_kritik(request):
    if request.method == 'POST':
        form = KritikSaranForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/kritikSaran/')
    else :
        form = KritikSaranForm()
    return render(request, 'form.html', {'form' : form})

def load_more(request):
    total_item = int(request.GET.get('total_item'))     
    limit = 3
    kritik_saran = list(KritikSaran.objects.values()[total_item:total_item+limit])
    data = {
        'KritikSaran': kritik_saran
    }
    return JsonResponse(data=data)

def get_kritik(request):
    kritik = KritikSaran.objects.all()
    data = serializers.serialize('json', kritik)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def add_kritik_saran(request):
    if not request.user.is_authenticated:
        return JsonResponse({"error": "Unauthorized"}, status=401)

    if request.method == 'POST':

        data = json.loads(request.body)

        nama = data["nama"]
        kritik_saran = data["kritik_saran"]
        waktu_post = data['waktu_post']

        kritik = KritikSaran(nama=nama, kritik_saran=kritik_saran, waktu_post=waktu_post)

        kritik.save()

        return JsonResponse({"status": "success"}, status=200)
    
    else:
        return JsonResponse({"status": "error"}, status=400)
