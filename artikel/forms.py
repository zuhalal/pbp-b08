from django import forms
from .models import Article

class DateInput(forms.DateInput):
    input_type = 'date'

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = "__all__"
        widgets ={'date_published': DateInput()}
        
    title = forms.CharField(widget = forms.Textarea(attrs={'placeholder': 'Tambahkan judul artikel'}))
    body = forms.CharField(widget = forms.Textarea(attrs={'placeholder': 'Tambahkan isi artikel'}))