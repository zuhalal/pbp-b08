# Create your views here.
from django.shortcuts import get_object_or_404, render
from .models import Article
from .forms import ArticleForm
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json

#create view
def index(request):
    artikel = Article.objects.all()
    response = {'artikel': artikel}
    return render(request, 'indexArtikel.html', response)

@login_required(login_url='login')
def add_artikel(request):
    if request.method == 'POST':
        form = ArticleForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/artikel/')
    else:
        form = ArticleForm()
    return render(request, 'formArtikel.html', {'formArtikel': form})

def detail_artikel(request, id):
    detail = Article.objects.all().get(id=id)
    response = {'detail' : detail}
    return render(request, 'tampilan_artikel.html', response)

def get_artikel(request):
    artikel = Article.objects.all()
    data = serializers.serialize('json', artikel)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def add_artikel(request):
    if not request.user.is_authenticated:
        return JsonResponse({"error": "Unauthorized"}, status=401)

    if request.method == 'POST':

        data = json.loads(request.body)

        author = data["author"]
        title = data["title"]
        body = data["body"]
        date = data["date"]

        artikel = Article(author=author, title=title, body=body, date=date)

        artikel.save()

        return JsonResponse({"status": "success"}, status=200)
    
    else:
        return JsonResponse({"status": "error"}, status=400)

