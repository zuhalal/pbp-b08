from django.urls import path
from .views import add_artikel, detail_artikel, index, get_artikel

urlpatterns = [
    path('', index, name='index'),
    path('form', add_artikel, name='form'),
    path('detail/<int:id>', detail_artikel, name='detail'),
    path('json', get_artikel, name='json'),
    path('addArtikel', add_artikel, name='add-artikel'),
]