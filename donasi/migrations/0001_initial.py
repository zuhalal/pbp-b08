# Generated by Django 3.2.8 on 2021-12-28 13:37

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Donasi',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('deskripsi', models.TextField(max_length=500)),
                ('link_gambar', models.TextField(max_length=500)),
                ('penggalang', models.CharField(max_length=100)),
                ('penerima', models.CharField(max_length=100)),
                ('target', models.IntegerField()),
                ('due_date', models.DateField()),
                ('link_donasi', models.TextField(default='')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='author', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
