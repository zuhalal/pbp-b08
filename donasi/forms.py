from django.forms import widgets
from donasi.models import Donasi
from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'

class DonasiForm(forms.ModelForm):
    class Meta:
        model = Donasi
        fields = '__all__'
        exclude = ('author',) 
        widgets ={'due_date': DateInput()}