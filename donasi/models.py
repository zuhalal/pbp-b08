from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE
from cloudinary.models import CloudinaryField
# Create your models here.

class Donasi(models.Model):
    def __str__(self):
        return str(self.author) + " - " + str(self.title)

    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name="author",blank=True, null=True)
    title = models.CharField(max_length=100)
    deskripsi = models.TextField(max_length=500)
    link_gambar = models.TextField(max_length=500)
    penggalang = models.CharField(max_length=100)
    penerima = models.CharField(max_length=100)
    target = models.IntegerField()
    due_date = models.DateField()
    link_donasi = models.TextField(default="")
