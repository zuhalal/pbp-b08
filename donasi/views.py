from django.http import response
from django.shortcuts import render
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import (get_object_or_404, render, HttpResponseRedirect)
from django.core import serializers
from donasi.forms import DonasiForm
from donasi.models import Donasi
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
# Create your views here.
from cloudinary.forms import cl_init_js_callbacks
from django.views.decorators.csrf import csrf_exempt
import json
from django.utils.dateparse import parse_date

def index(request):
    all_donasi = Donasi.objects.all()
    form = DonasiForm()
    donasiUser = []
    for donasi in all_donasi:
        if (donasi.author == request.user):
            donasiUser.append(donasi)

    response = {
        'donasi': all_donasi,
        'donasiOwner': donasiUser,
        'form': form
    }
    
    return render(request, 'index_donasi.html', response)

def detail_donasi(request, id):
    donasi = Donasi.objects.all().get(id=id)
    response = {
        'details' : donasi
    }
    return render(request, 'detail_donasi.html', response)

@csrf_exempt
def add_donasi(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/donasi")
    form = DonasiForm()

    if request.method == "POST":
        form  = DonasiForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.author = request.user
            obj.save()
            return HttpResponseRedirect("/donasi#myDonasi")

    response = {'form': form}
    return render(request, 'add_donasi.html', response)

@csrf_exempt
def edit_donasi(request, id):
    context ={}

    obj = get_object_or_404(Donasi, id = id)

    if request.user != obj.author:
            return HttpResponseRedirect("/donasi")

    form = DonasiForm(instance = obj)

    if request.method == "POST":
        form  = DonasiForm(request.POST, instance=obj)
        if form.is_valid() and request.method == "POST":
            form.save()
            return HttpResponseRedirect("/donasi#myDonasi")
        
    context["form"] = form

    return render(request, "edit_donasi.html", context)

@csrf_exempt
def delete_donasi(request, id):
    obj = get_object_or_404(Donasi, id = id)
    
    if request.method == "POST":
        # delete object
        obj.delete()
        return JsonResponse({"status": "success"}, status=200)

    return JsonResponse({"error": "Delete Failed"}, status=400)

@csrf_exempt
def get_all_donasi(request):
    donasi = Donasi.objects.all()
    data = serializers.serialize('json', donasi)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def get_my_donasi(request):
    all_donasi = Donasi.objects.all()

    donasiUser = []
    for donasi in all_donasi:
        #print(donasi.author)
        if (donasi.author == request.user):
            donasiUser.append(donasi)

    data = serializers.serialize('json', donasiUser)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def add_donasi_api(request):
    if not request.user.is_authenticated:
        return JsonResponse({"error": "Unauthorized"}, status=401)

    if request.method == 'POST':

        data = json.loads(request.body)

        title = data["title"]
        deskripsi = data["deskripsi"]
        link_gambar = data['link_gambar']
        penggalang = data['penggalang']
        penerima = data['penerima']
        target = data['target']
        due_date = data['due_date']
        link_donasi = data['link_donasi']

        donasi = Donasi(author=request.user, title=title, deskripsi = deskripsi, link_gambar=link_gambar, penggalang=penggalang, penerima=penerima, target=target, due_date=due_date, link_donasi=link_donasi)

        donasi.save()

        return JsonResponse({"status": "success"}, status=200)
    
    else:
        return JsonResponse({"status": "error"}, status=400)

@csrf_exempt
def edit_donasi_api(request, id):
    obj = get_object_or_404(Donasi, id = id)

    if request.user != obj.author:
        return JsonResponse({"error": "Unauthorized"}, status=401)

    if request.method == "POST":
        data = json.loads(request.body)
        new_title = data["title"]
        new_deskripsi = data["deskripsi"]
        new_link_gambar = data["link_gambar"]
        new_penggalang = data["penggalang"]
        new_penerima = data["penerima"]
        new_target = data["target"]
        new_due_date = parse_date(data["due_date"])
        new_link_donasi = data["link_donasi"]

        obj.author = request.user
        obj.title = new_title
        obj.deskripsi = new_deskripsi
        obj.link_gambar = new_link_gambar
        obj.penggalang = new_penggalang
        obj.penerima = new_penerima
        obj.target = new_target
        obj.due_date = new_due_date
        obj.link_donasi = new_link_donasi
        obj.save()
        return JsonResponse({"status": "success"}, status=200)
    
    return JsonResponse({"error": "failed"}, status=400)
