from donasi.views import add_donasi, add_donasi_api, edit_donasi, edit_donasi_api, get_my_donasi, index, detail_donasi, delete_donasi, get_all_donasi
from django.urls import path

urlpatterns = [
    path('', index, name='index_donasi'),
    path('detail/<int:id>', detail_donasi, name='detail_donasi'),
    path('delete/<int:id>', delete_donasi, name='delete_donasi'),
    path('all_donasi', get_all_donasi, name='get_all_donasi'),
    path('my_donasi', get_my_donasi, name='get_my_donasi'),
    path('add', add_donasi, name='add_donasi'),
    path('addAPI', add_donasi_api, name='add_donasiAPI'),
    path('edit/<int:id>', edit_donasi, name='edit_donasi'),
    path('editAPI/<int:id>', edit_donasi_api, name='edit_donasAPI'),
]
